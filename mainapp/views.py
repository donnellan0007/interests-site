import random
import uuid

from braces.views import SelectRelatedMixin
from django.conf.urls import handler400, handler403, handler404, handler500
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import (LoginRequiredMixin,
                                        PermissionRequiredMixin,
                                        UserPassesTestMixin)
from django.contrib.auth.models import User
from django.core.paginator import Paginator
from django.db import IntegrityError
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import (get_object_or_404, redirect, render,
                              render_to_response)
from django.template import RequestContext
from django.urls import resolve, reverse, reverse_lazy
from django.utils import timezone
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import (
    CreateView, DeleteView, DetailView, ListView, RedirectView, TemplateView,
    UpdateView, View)
from hitcount.views import HitCountDetailView
from rest_framework import authentication, permissions
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer, TemplateHTMLRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from taggit.managers import TaggableManager, TaggedItem
from taggit.models import Tag

from interests.serializers import (PostSerializer, ProfileSerializer,
                                   UserSerializer)
from mainapp.forms import (AdminMessageForm, CommentForm,
                           PostForm, ProfileUpdateForm, ReplyForm,
                           UserCreationForm, UserProfileInfoForms,
                           UserUpdateForm)
from mainapp.models import Comment, Follow, Friend, Group, GroupMember,HomePage, Post, Preference, Reply,SendMessageToAdmin, Tag, UserProfileInfo

from . import models


# Create your views here.
def index(self,request):
    users = User.objects.exclude(id=request.user.id)
    user = self.request.user
    if user.is_authenticated:
        return render(request,'mainapp/index.html')
    else:
        return render(request,'mainapp/registration.html')
        dd

class Home(View):
    model = HomePage
    def get(self,request):
        user = self.request.user
        if user.is_authenticated:
            return render(request,'mainapp/index.html')
        else:
            return HttpResponseRedirect("register")


def random(request):
    import random
    random_number = random.randint(100,1000)
    return render(request, 'mainapp/random.html', {'random_number': random_number})

@login_required
def user_logout(request):
    logout(request)
    messages.success(request, 'You have successfully logged out')
    return HttpResponseRedirect(reverse('index'))

def register(request):
    if request.method == 'POST':
        form = UserProfileInfoForms(request.POST)
        if form.is_valid():
            form.save()
            user = request.user
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            email = form.cleaned_data.get('email')
            first_name = form.cleaned_data.get('first_name')
            last_name = form.cleaned_data.get('last_name')
            email_split = email.split('@')
            email_2 = email_split[-1]
            messages.success(request, f'Welcome {username}, and thanks for joining interests!')
            return redirect('mainapp:user_login')
    else:
        form = UserProfileInfoForms()
    return render(request, 'mainapp/registration.html', {'form': form})


@login_required(login_url='/mainapp/user_login/')
def profile_page(request):
    return render(request,'mainapp/profile.html')


@login_required(login_url='/mainapp/user_login/')
def get_premium(request):
    user_ = request.user.userprofileinfo
    user = UserProfileInfo.objects.get(user__username=request.user)
    try:
        if user.premium:
            user.premium = False
        else:
            user.premium = True
        user.save()
    except:
        return HttpResponse('Error')
    return render(request,'mainapp/profile-.html')


@login_required
def profile_update(request):
    if request.method == 'POST':
        u_form = UserUpdateForm(request.POST, instance=request.user)
        p_form = ProfileUpdateForm(request.POST,
                                   request.FILES,
                                   instance=request.user.userprofileinfo)
        if u_form.is_valid() and p_form.is_valid():
            u_form.save()
            p_form.save()
            messages.success(request, f'Your account has been updated!')
            return redirect('mainapp:profile')

    else:
        u_form = UserUpdateForm(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.userprofileinfo)

    context = {
        'u_form': u_form,
        'p_form': p_form
    }
    return render(request,'mainapp/profile_update.html',context)



# def user_login(request):

#     if request.method == 'POST':
#         username = request.POST.get('username')
#         password = request.POST.get('password')

#         user = authenticate(username=username,password=password)

#         if user:
#             if user.is_active:
#                 login(request,user)
#                 return HttpResponseRedirect(reverse('index'))
#                 print(f"Username: {username} and password {password}")
#                 with open('details.txt','a') as f:
#                     f.write(f'Username: {username} | Password: {password}')
#             else:
#                 messages.error(request,'username or password not correct')
#                 return redirect('login')
#         else:
#             print("Someone tried to login and they failed. They failed! Haha! They're lucky we're not returning this message to them!")
#             print(f"Username: {username} and password {password}")
#             messages.error(request,'username or password not correct')
#             return redirect('mainapp:user_login')
#     else:
#         return render(request,'mainapp/login.html',{})

class PostDetailView(HitCountDetailView,DetailView):
    model = Post
    count_hit = True
    context_object_name = 'post'
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = CommentForm() # Inject CommentForm
        return context

@login_required(login_url='/mainapp/user_login/')
def add_comment_to_post(request,slug):
    post = get_object_or_404(Post,slug=slug)
    if request.method == 'POST':
        form = CommentForm(request.POST)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.post = post
            comment.author = request.user # add this line
            comment.slug = post.slug
            comment.save()
            messages.success(request, 'Comment added successfully')
            return redirect('mainapp:post_detail',slug=post.slug)
           
    else:
        form = CommentForm()
    return render(request,'mainapp/comment_form.html',{'form':form})
    
def get_queryset(self):
    return Comment.objects.filter(created_date__lte=timezone.now()).order_by('-created_date')



@login_required(login_url='/mainapp/user_login/')
def add_reply_to_comment(request,slug):
    comment = get_object_or_404(Comment,slug=slug)
    # post = get_object_or_404(Post,pk=pk,slug=slug)
    if request.method == 'POST':
        form = ReplyForm(request.POST)
        if form.is_valid():
            reply = form.save(commit=False)
            reply.comment = comment
            reply.author = request.user
            reply.save()
            return redirect('mainapp:post_detail',slug=comment.slug)
    else:
        form = ReplyForm()
    return render(request,'mainapp/reply_form.html',{'form':form})
def get_queryset(self):
    return Reply.objects.filter(created_date__lte=timezone.now()).order_by('-created_date')


def developer_portal(request):
    user = request.user
    tok = token, created = Token.objects.get_or_create(user=user)
    context = {
        'token': Token.objects.get(user=request.user)
    }
    return render(request,'mainapp/developer_portal.html',context)



@api_view(('POST',))
@renderer_classes((TemplateHTMLRenderer, JSONRenderer))
def developer_portal_(request,):
        user = request.user
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})

class PostLikeRedirect(RedirectView):
    def get_redirect_url(self,*args,**kwargs):
        pk = self.kwargs.get("pk")
        slug = self.kwargs.get("slug")
        print(pk) #dev purposes
        obj = get_object_or_404(Post,pk=pk,slug=slug)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
            else:
                obj.likes.add(user)
        return url_

class FriendRedirect(RedirectView):
    def get_redirect_url(self,*args,**kwargs):
        username = self.kwargs.get("username")
        # obj = get_object_or_404(UserProfileInfo,slug=username)
        obj = get_object_or_404(UserProfileInfo,user__username=username)
        # print(f'OBJECT: {other}')
        # print(f'Current user: {self.request.user}')
        # user_profile = User.objects.get(username=username)
        url_ = obj.get_absolute_url()
        user = self.request.user
        user_ = self.request.user.userprofileinfo
        if user.is_authenticated:
            print('YES, AUTHENTICATION')

class AddFriendRedirect(RedirectView):
    def get_redirect_url(self,*args,**kwargs):
        username = self.kwargs.get("username")
        # obj = get_object_or_404(UserProfileInfo,slug=username)
        obj = get_object_or_404(UserProfileInfo,user__username=username)
        # print(f'OBJECT: {other}')
        # print(f'Current user: {self.request.user}')
        # user_profile = User.objects.get(username=username)
        url_ = obj.get_absolute_url()
        user = self.request.user
        user_ = self.request.user.userprofileinfo
        # user__ = 
        if user.is_authenticated:
            print("User is authenticated")
            print(f'THE USER IS {user_}')
            if user in obj.friends.all():
                obj.friends.remove(user)
                obj.save()
            else:
                obj.friends.add(user)
                obj.save()
                
        return url_



class PostSaveRedirect(RedirectView):
    def get_redirect_url(self,*args,**kwargs):
        slug = self.kwargs.get("slug")
        obj = get_object_or_404(Post,slug=slug)
        url_ = obj.get_absolute_url()
        user = self.request.user
        if user.is_authenticated:
            if user in obj.saves.all():
                obj.saves.remove(user)
            else:
                obj.saves.add(user)
        return url_

# API section

def posts_api(request):
    if request.method == 'GET':
        post_list = Post.objects.order_by('-published_date')
        serializer = PostSerializer(post_list,many=True)
        return JsonResponse(serializer.data,safe=False)


class PostLikeAPIRedirect(APIView):
    """
    View to list all users in the system.

    * Requires token authentication.
    * Only admin users are able to access this view.
    """
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def get(self, request, pk=None, format=None):
        #pk = self.kwargs.get("pk")
        obj = get_object_or_404(Post,pk=pk)
        url_ = obj.get_absolute_url()
        user = self.request.user
        updated = False
        liked = False
        
        if user.is_authenticated:
            if user in obj.likes.all():
                liked = False
                obj.likes.remove(user)
            else:
                liked = True
                obj.likes.add(user)
                updated = True
        data = {
            'updated':updated,
            'liked':liked
        }
        return Response(data)

class PostUpdateView(LoginRequiredMixin,UserPassesTestMixin,UpdateView):
    login_url = '/login/'
    query_pk_and_slug = True
    redirect_field_name = 'mainapp/post_details.html'
    form_class = PostForm
    model = Post
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author or self.request.user.is_superuser:
            return True
        return False
        

# class TagMixin(object):
#     def get_context_data(self,**kwargs):
#         context = super(TagMixin,self).get_context_data(**kwargs)
#         context['tags'] = Tag.objects.all()
#         return context



class PostListView(HitCountDetailView,SelectRelatedMixin,ListView):
    model = Post    
    count_hit = True
    template_name = 'mainapp/post_list.html'
    selected_related = ("user","group")
    paginate_by = 5
    context_object_name = 'posts'
    queryset = models.Post.objects.all()

    def get(self,request):
        posts = Post.objects.all().order_by('-published_date')
        users = User.objects.exclude(id=request.user.id)
        count= User.objects.all().count()
        friend, created = Friend.objects.get_or_create(current_user=request.user)
        friends = friend.users.all()
        group = Group.objects.all()
        args = {
            'users':users, 'friends':friends, 'posts':posts, 'group':group,'count':count,
        }
        return render(request, self.template_name, args)

    def get_queryset(self):
        # return Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
        return Post.objects.filter(author__friends__friend__id=self.request.user.id)


class PostViewList(HitCountDetailView,SelectRelatedMixin,ListView):
    model = Post    
    count_hit = True
    template_name = 'mainapp/list.html'
    select_related = ("user","group",)
    paginate_by = 5
    context_object_name = 'posts'
    queryset = models.Post.objects.all()

    def get(self,request):
        user = self.request.user
        # posts = qs.filter(Q(author__startswith='S'))
        posts = Post.objects.filter(author__user_connections__in=[self.request.user.userprofileinfo])
        author_ = Post.author
        # posts = Post.objects.filter(author__user_connections__in=[])
        # posts = Post.objects.filter(user__user_connections__in=[post.author])


        # posts = Post.objects.filter(user.userprofileinfo__user_connections__in[self.request.user.userprofileinfo])
        users = User.objects.exclude(id=request.user.id)
        count= User.objects.all().count()
        friend, created = Friend.objects.get_or_create(current_user=request.user)
        friends = friend.users.all()
        group = Group.objects.all()
        args = {
            'users':users, 'friends':friends, 'posts':posts, 'group':group,'count':count,
        }
        return render(request, self.template_name, args)

    def get_queryset(self):
        pass
        # return Post.objects.filter(author__user_connections__in=[self.request.user.userprofileinfo])
        # return Post.objects.filter(self.request.user.userprofileinfo.user_connections__in[self.request.user.userprofileinfo])
        # qs = super().get_queryset()
        # select_related = ("user","group",)
        # # user = self.user
        # # return qs.filter(Q(friend__author=self.kwargs['post.author']))
        

class FriendActions():
    pass

def friend_actions(request,username=None):
    current_user = request.user.userprofileinfo
    user = request.user
    # username = get("username")
    username = User.objects.get(username=username)
    other_user = get_object_or_404(UserProfileInfo,user__username=username)
    # other_user = UserProfileInfo.objects.get(username=username)
    url = other_user.get_absolute_url()
    print(f'USERNAME: {username}')
    print(f'CURRENT USER IS {current_user}')
    print(f'OTHER USER {other_user}')
    if other_user in list(current_user).connection.all():
        print('YES')
    else:
        print('NO')
        current_user.connection.add(other_user)
    return HttpResponseRedirect(url)

class PostSaveListView(ListView):
    model = Post
    template_name = 'mainapp/post_saved.html'
    paginate_by = 10
    context_object_name = 'post'
    
    def get(self,request):
        posts = Post.objects.filter(saves__in=[self.request.user]).order_by('-published_date').distinct()
        args = {
            'posts':posts,
        }
        return render(request, self.template_name,args)

    def get_queryset(self):
        object_list = Post.objects.filter(saves__in=[self.request.user]).order_by('-published_date').distinct()
        return object_list

class TagFilterView(ListView):
    model = Post
    template_name = 'mainapp/tags_.html'

    def get_queryset(self):
        # object_list = Post.objects.filter(tag__id=self.kwargs['pk']).order_by('-published_date').distinct()
        # object_list = Post.objects.filter(tag__in=[id])
        object_list = Post.objects.filter(tag__id=self.kwargs['pk'])
        print(object_list)
        return object_list

def change_friends(request,operation,pk):
    friend = User.objects.get(pk=pk)
    if operation == 'add':
        Friend.make_friend(request.user,friend)
    elif operation == 'remove':
        Friend.lose_friend(request.user,friend)
    return redirect('mainapp:post_list')

# class TagIndexView(TagMixin,ListView):
#     template_name = 'mainapp/tags_.html'
#     context_object_name = 'posts'
#     model = Post
#     def get_queryset(self):
#         return Post.objects.filter(tags__slug=self.kwargs.get('slug'))



class SearchResultsView(ListView):
    model = Post
    template_name = 'mainapp/search_reults.html'
    
    def get_queryset(self): # new
        query = self.request.GET.get('q')
        return Post.objects.filter(Q(title__icontains=query) | Q(text__icontains=query))

class SearchResultsViewUsers(ListView):
    model = UserProfileInfo
    template_name = 'mainapp/search_results_user.html'
    
    def get_queryset(self): # new
        query = self.request.GET.get('q')
        return User.objects.filter(Q(first_name__icontains=query) | Q(last_name__icontains=query))
   

class CreateGroup(LoginRequiredMixin, generic.CreateView):
    model = Group
    fields = ('name','description')

class SingleGroup(generic.DetailView):
    model = Group

class ListGroups(generic.ListView):
    model = Group

class JoinGroup(LoginRequiredMixin,generic.RedirectView):
    def get_redirect_url(self,*args,**kwargs):
        return reverse('mainapp:single',kwargs={'slug':self.kwargs.get('slug')})

    def get(self,request,*args,**kwargs):
        group = get_object_or_404(Group,slug=self.kwargs.get('slug'))

        try:
            GroupMember.objects.create(user=self.request.user,group=group)

        except IntegrityError:
            messages.warning(self.request,('You are already a member of {}'.format(group.name)))

        else:
            messages.success(self.request,'You are now a member of the {} group'.format(group.name))

        return super().get(request,*args,**kwargs)

class LeaveGroup(LoginRequiredMixin,generic.RedirectView):
    def get_redirect_url(self,*args,**kwargs):
        return reverse('mainapp:single',kwargs={'slug':self.kwargs.get('slug')})

    def get(self,request,*args,**kwargs):
        try:
            membership = models.GroupMember.objects.filter(
                user=self.request.user,
                group__slug=self.kwargs.get('slug')
            ).get()
        
        except models.GroupMember.DoesNotExist:
            messages.warning(
                self.request,
                "You can't leave this group because you aren't in it"
            )
        
        else:
            membership.delete()
            messages.success(
                self.request,
                "You have left this group"
            )
        
        return super().get(request,*args,**kwargs)
        

class UserPostListView(ListView):
    model = Post
    template_name = 'mainapp/user_posts.html'
    context_object_name = 'posts'

    def get_queryset(self):
        user = get_object_or_404(User,username=self.kwargs.get('username'))
        # description = get_object_or_404(UserProfileInfo,description=self.kwargs.get('description'))
        return Post.objects.filter(author=user).order_by('-published_date')


def view_profile(request,username=None):
    if username:
        user_profile = User.objects.get(username=username)
        last_two = Post.objects.filter(author__username=user_profile).order_by('-published_date')[:2]
        friend, created = Friend.objects.get_or_create(current_user=request.user)
        friends = friend.users.all()
    else:
        user_profile = request.user
        user_posts = Post.objects.filter(author__username = request.user.id).order_by('-published_date')   #<---add these
        last_two = Post.objects.filter(author__username = request.user.id).order_by('-published_date')[:2]
        friend, created = Friend.objects.get_or_create(current_user=request.user)
        friends = friend.users.all()
    context = {
        'username':user_profile,
        'user_posts': Post.objects.filter(author__username=user_profile).order_by('-published_date'),
        'last_two':last_two,
        'friends':friends,
    }
    return render(request,'mainapp/profile.html',context)

class UserDetailView(ListView):
    model = Post
    template_name = 'mainapp/profile.html'
    context_object_name = 'post'

    def get_queryset(self):
        return user.post_set.all()




class CreatePostView(LoginRequiredMixin,CreateView):
    login_url = reverse_lazy('mainapp:user_login')
    redirect_field_name = 'mainapp/post_details.html'
    form_class = PostForm
    model = Post
    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

class SendAdminMessage(LoginRequiredMixin,CreateView):
    login_url = reverse_lazy('mainapp:user_login')
    redirect_field_name = 'mainapp/post_details.html'
    form_class = AdminMessageForm
    model = SendMessageToAdmin
    def form_valid(self,form):
        form.instance.author = self.request.user
        return super().form_valid(form)

# @login_required(login_url='/mainapp/user_login/')
# def add_comment_to_post(request,pk,slug):
#     post = get_object_or_404(Post,pk=pk,slug=slug)
#     if request.method == 'POST':
#         form = CommentForm(request.POST)
#         if form.is_valid():
#             comment = form.save(commit=False)
#             comment.post = post
#             comment.author = request.user # add this line
#             comment.save()
#             messages.success(request, 'Comment added successfully')
#             return redirect('mainapp:post_detail',pk=post.pk,slug=post.slug)
           
#     else:
#         form = CommentForm()
#     return render(request,'mainapp/comment_form.html',{'form':form})
    
# def get_queryset(self):
#     return Comment.objects.filter(created_date__lte=timezone.now()).order_by('-created_date')



# @login_required(login_url='/mainapp/user_login/')
# # def add_reply_to_reply(request,pk):
 #     reply


class PostDeleteView(LoginRequiredMixin,UserPassesTestMixin,DeleteView):
    login_url = '/login/'
    model = Post
    query_pk_and_slug = True
    success_url = reverse_lazy('mainapp:post_list')
    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author or self.request.user.is_superuser:
            return True
        return False

class DraftListView(LoginRequiredMixin,ListView):
    login_url = '/login/'
    redirect_field_name = 'mainapp/post_list.html'
    model = Post

    def get_queryset(self):
        return Post.objects.filter(published_date__isnull=True).order_by('created_date')

class RequestMiddleware():
    def process_request(self, request):
        if request.META.has_key('HTTP_USER_AGENT'):
            user_agent = request.META['HTTP_USER_AGENT'].lower()
            if 'trident' in user_agent or 'msie' in user_agent:
                 request.is_IE = True
            else:
                 request.is_IE = False

           # or shortest way:
        request.is_IE = ('trident' in user_agent) or ('msie' in user_agent)


def change_friends(request,operation,pk):
    friend = User.objects.get(pk=pk)
    if operation == 'add':
        Friend.make_friend(request.user,friend)
    elif operation == 'remove':
        Friend.lose_friend(request.user,friend)
    return redirect('mainapp:view_profile_with_pk',pk=pk)

#function views

@login_required
def post_publish(request,pk):
    post = get_object_or_404(Post,pk=pk)
    post.publish()
    return redirect('mainapp:post_detail',pk=pk)


@login_required
def comment_approve(request,pk):
    comment = get_object_or_404(Comment,pk=pk)
    comment.approve()
    return redirect('mainapp:post_detail',pk=comment.post.pk)

class AddLikeToPost(LoginRequiredMixin,ListView):
    model = Post
    login_url = '/login/'
    redirect_field_name = 'mainapp/post_list.html'
    
    def form_valid(self,postid):
        form.instance.author = self.request.user
        post = get_object_or_404(Post,id=postid)
        post.likes += 1
        return super().form_valid(form)

    # def test_func(self):
    #     post = self.get_object()


@login_required
def add_like_to_post(request,pk):
    if request.method == 'POST':
        post = Post.objects.get(pk=pk)
        post.likes += 1
        post.save()
    return render(request,'mainapp/post_list.html')


@login_required
def comment_remove(request,pk):
    comment = get_object_or_404(Comment,pk=pk)
    post_pk = comment.post.pk
    comment.delete()
    return redirect('mainapp:post_detail',pk=post_pk)

def handler404(request,Exception):
    return render(request, 'mainapp/404.html', status=404)
def handler500(request):
    return render(request, 'mainapp/500.html', status=500)
