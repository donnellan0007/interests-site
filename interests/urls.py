"""interests URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf import settings
from django.urls import path,include
from django.conf.urls import url
from django.conf.urls.static import static
from avatar import urls
from mainapp import views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.contrib.auth.models import User
from mainapp.models import Post
from .serializers import PostSerializer,UserSerializer,ProfileSerializer
from rest_framework.authtoken.models import Token
from rest_framework import routers, serializers, viewsets
from mainapp.models import UserProfileInfo
from rest_framework import routers
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.response import Response

from django.contrib.auth.models import User
from rest_framework.decorators import action
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken import views as drf_views

# ViewSets define the view behavior.
class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

class ProfileViewSet(viewsets.ModelViewSet):
    queryset = UserProfileInfo.objects.all()
    serializer_class = ProfileSerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)
    

class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    # authentication_classes = (TokenAuthentication,)
    # permission_classes = (IsAuthenticated,)

# Routers provide an easy way of automatically determining the URL conf.
router = routers.DefaultRouter()
router.register(r'users', UserViewSet)
router.register(r'posts',PostViewSet)
router.register(r'profiles',ProfileViewSet)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.Home.as_view(),name='index'),
    path('mainapp/',include('mainapp.urls')),
    path('logout/',views.user_logout,name='logout'),
    path('profile/',views.profile_page,name='profile'),
    path('api-auth/', include('rest_framework.urls')),
    path('api-token-auth/', drf_views.obtain_auth_token, name='api-tokn-auth'), 
    path('', include(router.urls)),
    path('avatar/',include('avatar.urls')),
    path('emoji/',include('emoji.urls')),
    path('register/',views.register,name='register'),
    path('hitcount/', include('hitcount.urls', namespace='hitcount')),
    path('tinymce/', include('tinymce.urls')),
    path('', include('django_private_chat.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
