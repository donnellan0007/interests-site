from rest_framework import routers, serializers, viewsets
# model imports
from django.contrib.auth.models import User
from mainapp.models import Post,UserProfileInfo
from rest_framework.authtoken.models import Token

# Serializers define the API representation.
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'is_staff','password']
        extra_kwargs = {
            'password':{'write_only':True,'required':True}
        }
        
class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfileInfo
        fields = ['user','description','website','banner','image','joined_date','gender','verified','is_private','dark_mode','slug','colour','moderator','skills','owner','premium','friends','is_developer']

class PostSerializer(serializers.ModelSerializer):
    class Meta:
        model = Post
        fields = ['title','author','text','slug','created_date','tag']